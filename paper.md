# Use Of Service Oriented Architecture For Improving Performance And Scaling Issues

## Abstract

After adopting a service-oriented architecture, performance may decrease (SOA). Applying SOA to systems for processing massive amounts of data becomes more challenging as a result. This study discusses distinctive SOA characteristics that significantly affect performance. It examines numerous accepted solutions to these issues and shows why systems for processing large amounts of data need a framework to create an SOA.

## Table Of Contents

   1. Introduction.

      1.1 Service Oriented Architecture.

      1.2 Performance.
    
    2. SOA Performance Hotspot

       2.1 Integration of Heterogeneous Technologies.

       2.2 Loose Coupling.

     3. Current Approaches

       3.1. Hardware.

       3.2. Compression.

       3.3. Service Granularity.

       3.4. Degree of Loose Coupling

    4. Applying SOA to Batch Processing Systems.

    5. Conclusion.

    6. References.


## 1. Introduction

In a software design methodology known as service-oriented architecture, application components provide services to the other components via a communication protocol via a network (SOA). Suppliers and different technologies have no bearing on their principles. Many services communicate with one another in service-oriented architecture by exchanging data or organizing an action among several services.  Implementing an SOA has certain impacts on these non-functional requirements that ought to be considered beforehand.

Performance is a crucial non-functional requirement for an IT system and is essential to its usability and adoption. Performance problems are frequently discovered at a late point in the development process since performance tests are typically not conducted until the system is already in place. Significant project risks ultimately result from the major changes to the architecture required to increase the system's performance. It is essential to be aware of performance issues and how to appropriately address them during the system design stage because the introduction of SOA can cause performance to suffer.

Particularly bulk data processing necessitates a high-performance architecture. Because they are centered on a request-response communication strategy, current methods for implementing an SOA employing web service technologies and infrastructures do not mesh well with a batch-processing paradigm.

The performance problems unique to SOA are described in this study along with the existing solutions. It drives home the requirement for a framework to incorporate a batch processing system in a landscape of service-oriented applications.

### 1.1 Service-Oriented Architecture

An architectural design pattern for creating application landscapes out of individual business components is called service-oriented architecture (SOA). Through the provision of their functionality in the form of services, these business components are merely connected.
A service hides all implementation details of the component providing the service and represents an abstract business view of the capability.
Between the service provider and the service customer, the definition of service serves as a contract. Services are called using a single mechanism that offers a platform-independent connection of the business components while concealing all of the technical aspects of the communication.

To increase the flexibility of enterprise systems, SOA divides the technical from the business components.

Making practical technological decisions to put an SOA's concepts into practice requires building one.
This covers the implementation of services, finding the exemplary service, and connecting them.
The technological choices that must be made to build an SOA efficiently are the main topic of this study.

### 1.2 Performance

Performance is a quality attribute of a software system and is crucial to the
acceptance of a developed system both by users and IT operations.
The performance of a system can be described by multiple metrics. The following metrics are relevant to the understanding of this paper:

• Response Time
The time it takes for the service consumer to receive a response from the
service provider

• Throughput
Number of requests a service provider can process in a fixed timeframe

• Latency
The time it takes that a service request to be received by the service provider and vice versa.

## 2. SOA Performance Hotspot

The various areas of a service-oriented architecture where performance problems frequently develop are described in this section. A distributed system has been developed using SOA concepts. Distinct departments' and even different organizations' services are hosted at various sites.
The shortcomings of a distributed system's performance.The marshaling of the data that the service consumer must send to the service provider, transmitting the data over the network, and the service provider's unmarshalling of the data.

In addition to these common problems with distributed systems, several characteristics of an SOA impair performance even further.

### 2.1 Integration of Heterogeneous Technologies

The integration of applications created using diverse technologies is one of the key objectives of the introduction of an SOA.
This is accomplished by utilizing particular middleware and intermediary communication protocols.
These protocols, like SOAP, are often based on XML. Because XML is such a verbose language, it adds a lot of meta-data to the message's actual payload. Due to the size of the resulting request, which is roughly 10 to 20 times greater than the corresponding binary form, the message's transmission time is significantly increased.

These messages must first be parsed by an XML parser before any processing can take place, which adds time to the operation.

## 2.2 Loose Coupling

The use of loose coupling is another feature of SOA that affects performance. By decreasing the dependence of its components on one another, loose coupling aims to boost the flexibility and maintainability of the application landscape. This means that users of services shouldn't assume anything about how those services are implemented, and vice versa.
As long as they implement the interface that the client expects, services become interchangeable.

When the following conditions are met, Engels and colleagues consider two components A and B to be loosely connected.

### • Information

Only the information required for properly utilizing the activities provided by component B is known by component A.
This covers the structure of the transferred data as well as the syntax and semantics of the interfaces.

### • Reliance on accessibility

Even when component B is unavailable or the link to component B is down, component A still delivers the implemented service.

### • Trust

Component B is independent of Component A's ability to fulfill prerequisites. For component A to adhere to post-conditions, component B is not necessary. There are several levels of coupling between services.

Among other things, performance costs work against loose coupling's advantages in flexibility and maintainability. The relationship between service users and service providers is not static.
Therefore, the service consumer must ascertain the proper end point of the service provider when the service is in use.
This can be accomplished by either routing the message inside the ESB or by the service consumer themselves searching for the appropriate service provider in a service repository before placing the call. Service users and service providers do not share the same data model, except for a very small number of basic data types.

As a result, data must be mapped between the data models used by the service provider and the service consumer.

## 3. Current Approaches

This section describes current approaches to the performance issues introduced in the previous section.

### 3.1. Hardware

The obvious solution to improve the processing time of a service is the utilization of faster hardware and more bandwidth. SOA performance issues are often neglected by suggesting that faster hardware or more bandwidth will solve this problem. However, it is often not feasible to add faster hardware in a late stage of the project because it involves more costs than initially planned.

### 3.2. Compression

The usage of XML as an intermediate protocol for service calls harms their transmission times over the network. The transmission time of service calls and responses can be decreased by compression. Simply compressing service calls and responses with gzip can do this. The World Wide Web Consortium (W3C) proposes a binary presentation of XML documents called binary XML to achieve more efficient transportation of XML over
networks. It must be pointed out that the utilization of compression adds the additional costs of
compressing and decompressing to the overall processing time of the service call.

### 3.3. Service Granularity
To reduce the communication overhead or the processing time of service, the service granularity should be reconsidered. Coarse-grained services reduce the communication overhead by achieving more with a single service call and should be the favored service design principle. However, the processing time of a coarse-grained service can pose a problem to a service consumer that only needs a fracture of the data provided by the service. To reduce the processing time it could be considered in this case to add a finer-grained service that provides only the needed data. It should be noted that merging multiple services to form a more coarse-grained service or splitting a coarse-grained service into multiple services to solve performance problems specific to a single service consumer reduces the reusability of the services for other service consumers.

### 3.4. Degree of Loose Coupling
The improvements in flexibility and maintainability gained by loose coupling are opposed to drawbacks on performance. Thus, it is crucial to find the appropriate degree of loose coupling. Hess et al. introduce the concept of distance to determine an appropriate degree of coupling between components. The distance of components is comprised of the
functional and technical distance. Components are functional distant if they share few functional similarities. Components are technical distant if they are of a different category. Categories classify different types of components like inventory components, process components, function components, and interaction components.
Distant components trust each other concerning the compliance of service levels to a lesser extent than near components do.

## 4. Applying SOA to Batch Processing Systems
How to apply the concepts of Service-Oriented Architecture to batch processing systems considering the arguments presented in section 2? A naive approach would be the utilization of web service technologies for these kinds of systems as well. However, because of the performance issues mentioned in this paper, this option would not scale for bulk processing of data with a batch-processing model.

An approach to transfer bulk data between web services per FTP. The SOAP messages transferred between the web services would only contain the necessary details on how to download the corresponding data
from an FTP server since this protocol is optimized for transferring huge files (Wichaiwong et al., 2007). This approach solves the technical aspect of efficiently transferring the input and output data but does not pose any solutions on how to
implement loose coupling and how to integrate heterogeneous technologies, the fundamental means of an SOA to improve the flexibility of an application landscape.

To integrate a batch processing system into a service-oriented application landscape several design decisions need to get addressed:

• How to implement loose coupling?

• What is the appropriate degree of loose coupling?

• What is the right service granularity?

• Which middleware technologies can be utilized for the integration of heterogeneous technologies?

• Who is responsible for data transformation?

• What data formats should be used?

## 5.Conclusion
This paper discusses several approaches to improving performance using  SOA which can be established.

The obvious approach is to utilize faster hardware and more network bandwidth. The compression of messages poses an option for reducing transmission times. Other approaches suggest reconsidering the service or architecture design. To decrease the communication overhead immanent in an SOA services should be coarse-grained. Is
the processing time of a coarse-grained service causing problems for a specific
service consumer, a finer-grained service should be added.

To apply the proper approach to performance issues it is vital to know the bottleneck of the system. Unfortunately, the measuring of system performance and the investigation of bottlenecks can be done only at a late stage in the development phase. SOA Performance models are trying to anticipate the performance behavior during the design phase but they are currently still under research. Improving the performance of a system always impacts other quality attributes. Adjusting the degree of loose coupling affects the flexibility of the system. Merging services to more coarse-grained services or splitting coarse-grained services into finer-grained services to solve performance issues of specific service consumers impacts the reusability of the services. Thus, it is vital to find the appropriate trade-off between performance and other quality attributes of the system like flexibility, maintainability, and reusability.

Batch processing systems in particular demand a high-performance implementation. To integrate these kinds of systems in a service-oriented application landscape several design decisions need to get addressed. For example, what is the appropriate degree of loose coupling and the right service granularity to achieve the required performance? Given that there are no obvious answers to these questions, there is a certain need for a framework for service-oriented processing of bulk data.

## 6.References

* SOAP Specification (2007), http://www.w3.org/TR/soap

* IBM Cloud Hub  https://www.ibm.com/cloud/learn/soa

* Medium https://medium.com/@SoftwareDevelopmentCommunity what-is-service-oriented-architecture-fa894d11a7ec

* https://www.techtarget.com/searchapparchitecture/definition/service-oriented-architecture-SOA

* https://www.sciencedirect.com/topics/computer-science/services-oriented-architecture